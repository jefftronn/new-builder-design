// Disable/Enable MLS Photos

$('.disable-mls-photos-switch').click(function () {
    if (!$(this).hasClass('active')) {
        showPopup('disable-mls-photos');
        $('.popup-overlay').addClass('cancel-disable-mls-photos');
    }
})

$('body').on('click', '.cancel-disable-mls-photos', function () {
    $('.disable-mls-photos-switch').addClass('active');
    $('.popup-overlay').removeClass('cancel-disable-mls-photos');
});