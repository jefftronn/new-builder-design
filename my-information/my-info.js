//MY INFORMATION PAGE

function addTestimonial() {
    $('.testimonials-wrapper').append(testimonialTemplate);
}

function removeTestimonial() {
    $('#testimonials-grid').remove();
}

function addPhoneAgent() {
    $('#phone-details-grid').clone().appendTo('#phone-wrapper');
};

function removePhoneAgent() {
    $('#phone-details-grid').remove();
}

function addEmailAgent() {
    $('#email-details-grid').clone().appendTo('#email-wrapper');
};

function removeEmailAgent() {
    $('#email-details-grid').remove();
}

function addAddressAgent() {
    $('#address-details-grid').clone().appendTo('#address-wrapper');
};

function removeAddressAgent() {
    $('#address-details-grid').remove();
}

function addPhoneAgentEdit() {
    $('#phone-details-grid-edit').clone().appendTo('#phone-wrapper-edit');
};

function removePhoneAgentEdit() {
    $('#phone-details-grid-edit').remove();
}

function addEmailAgentEdit() {
    $('#email-details-grid-edit').clone().appendTo('#email-wrapper-edit');
};

function removeEmailAgentEdit() {
    $('#email-details-grid-edit').remove();
}

function addAddressAgentEdit() {
    $('#address-details-grid-edit').clone().appendTo('#address-wrapper-edit');
};

function removeAddressAgentEdit() {
    $('#address-details-grid-edit').remove();
}

// function addTestimonial() {
//   $('#testimonial-grid').clone().appendTo('#testimonial-wrapper');
// }

function divClicked() {
    var divHtml = $(this).html();
    var editableText = $("<textarea />");
    editableText.val(divHtml);
    $(this).replaceWith(editableText);
    editableText.focus();
    // setup the blur event for this new textarea
    editableText.blur(editableTextBlurred);
}

function editableTextBlurred() {
    var html = $(this).val();
    var viewableText = $("<div>");
    viewableText.html(html);
    $(this).replaceWith(viewableText);
    // setup the click event for this new div
    viewableText.click(divClicked);
}

$(document).ready(function () {
    $("#testimonial-editable").click(divClicked);
    
    // Dropify for agent/company photo
    $('.dropify').dropify();
});


var testimonialTemplate = '<div class="testimonial"> <div class="info-group"> <div class="header"> <div class="header-label"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-star"><polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon></svg> <span>Testimonial</span> </div><div class="header-actions"> <div data-tippy="Remove Testimonial from Site" class="header-actions"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" onclick="removeTestimonial()" class="feather feather-trash-2 action-svg-delete"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg> </div></div></div><div class="data-collection"> <div class="input-group"> <div class="input-wrapper"> <label for="testimonial-first">First Name</label> <input type="text" id="testimonial-first" placeholder="Enter First Name"/> </div><div class="input-wrapper"> <label for="testimonial-last">Last Name</label> <input type="text" id="testimonial-last" placeholder="Enter Last Name"/> </div></div><div class="input-group m-t-15"> <div class="input-wrapper"> <label>Testimonial Text</label> <textarea type="text" id="testimonial-text-edit" placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip"></textarea> </div></div></div></div></div>';