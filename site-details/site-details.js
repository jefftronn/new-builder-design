// MLS TAB

// ADDING MULTIPLE MLS ONCLICK 

function appendCarret(trigger) {
    trigger.append('<svg class="caret-down" xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="6 9 12 15 18 9"></polyline></svg>');
}

function createDropdowns() {
    $('.dropdown').each(function () {
        if (!$(this).hasClass('dropdown-created')) {
            var dropdown = $(this);
            var button = $(dropdown).find('.dropdown-toggle');
            var trigger = this.querySelector('.dropdown-toggle');
            var menu = this.querySelector('.dropdown-menu').outerHTML;

            dropdown.addClass('dropdown-created');
            appendCarret(button);
            tippy(trigger, {
                content: menu,
                placement: 'bottom-start',
                interactive: true,
                interactiveBorder: 4,
                trigger: "click",
                arrow: false,
                distance: 5,
                onShow: function () {
                    $(dropdown).addClass('open');
                },
                onHide: function () {
                    $(dropdown).removeClass('open');
                    if ($(menu).children('.dropdown').length > 0) {
                        dropdownTip.disable();
                    }
                }
            });
        }
    });
}

function addMLS(){
  $('.mls-wrapper').append(mlsTemplate);
  createDropdowns();
};

function removeMLS() {
  $('#mls-details-grid').remove();
}

// BODY/HEAD INCLUDES TAB

// INITIATING CODEMIRROR AFTER CLICK EVENT SO IT CAN LOAD PROPERLY WITH ITS STYLES
var elementIsClicked = false;

function reloadTab() {
  if (elementIsClicked == false) {
    startTimer();
    elementIsClicked = true;
  }
  
  function startTimer() {
    setTimeout(initiateCodeMirror, 500);
  }

  function initiateCodeMirror() {
    var editor5 = CodeMirror (document.getElementById("site-head"), {
      mode: "htmlmixed",
      theme: "blackboard",
      lineNumbers: true,
      indentWithTabs: true
    });
  
    var editor6 = CodeMirror (document.getElementById("site-body"), {
      mode: "htmlmixed",
      theme: "blackboard",
      lineNumbers: true
    });
  }
}



var mlsTemplate = '<div id="mls-details-grid" class="info-group-col"> <div class="info-group"> <div class="header"> <div class="header-label"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-list"><line x1="8" y1="6" x2="21" y2="6"></line><line x1="8" y1="12" x2="21" y2="12"></line><line x1="8" y1="18" x2="21" y2="18"></line><line x1="3" y1="6" x2="3" y2="6"></line><line x1="3" y1="12" x2="3" y2="12"></line><line x1="3" y1="18" x2="3" y2="18"></line></svg> <span>MLS Information</span> </div><div data-tippy="Remove MLS from Site" class="header-actions"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewbox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" onclick="removeMLS()" class="feather feather-trash-2 action-svg-delete"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg> </div></div><div class="data-collection black"> <div class="input-group"> <div class="input-wrapper"> <label for="state">State</label> <div class="dropdown" id="state"> <button type="button" class="dropdown-toggle jc-between" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="toggle-label">Select State</span> </button> <ul class="dropdown-menu"> <li class="dropdown-item"> <label class="radio item-label"> AL <input type="radio" name="state"> <span class="radio-indicator"></span> </label> </li><li class="dropdown-item"> <label class="radio item-label"> AR <input type="radio" name="state"> <span class="radio-indicator"></span> </label> </li><li class="dropdown-item"> <label class="radio item-label"> CA <input type="radio" name="state"> <span class="radio-indicator"></span> </label> </li><li class="dropdown-item"> <label class="radio item-label"> CO <input type="radio" name="state" checked=""> <span class="radio-indicator"></span> </label> </li></ul> </div></div><div class="input-wrapper"> <label for="mls">MLS</label> <div class="dropdown" id="mls"> <button type="button" class="dropdown-toggle jc-between" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="toggle-label">Select MLS</span> </button> <ul class="dropdown-menu"> <li class="dropdown-item"> <label class="radio item-label"> Wasatch <input type="radio" name="mls" checked> <span class="radio-indicator"></span> </label> </li><li class="dropdown-item"> <label class="radio item-label"> Uinta <input type="radio" name="mls"> <span class="radio-indicator"></span> </label> </li></ul> </div></div></div><div class="input-group m-t-15"> <div class="input-wrapper"> <label for="mls-office-id">Office ID</label> <input type="text" id="mls-id" placeholder="123456789"> </div><div class="input-wrapper"> <label for="mls-agent-id">MLS Agent ID</label> <input type="text" id="mls-agent-id" placeholder="123456789"> </div></div></div></div></div>';