// Sort Pages in Custom Order

$(".draggable-list").sortable({
    group: 'draggable-list',
    pullPlaceholder: false,
    // animation on drop
    onDrop: function ($item, container, _super) {
        var $clonedItem = $('<li/>').css({ height: 0 });
        $item.before($clonedItem);
        $clonedItem.animate({ 'height': $item.height() });

        $item.animate($clonedItem.position(), function () {
            $clonedItem.detach();
            _super($item, container);
        });
        hasChildren();
    },

    // set $item relative to cursor position
    onDragStart: function ($item, container, _super) {
        var offset = $item.offset(),
            pointer = container.rootGroup.pointer;

        adjustment = {
            left: pointer.left - offset.left,
            top: pointer.top - offset.top
        };

        _super($item, container);
    },
    onDrag: function ($item, position, e) {
        $item.css({
            left: position.left - adjustment.left,
            top: position.top - adjustment.top
        });
    },
    onMousedown: function ($item, _super, event) {
        if (!event.target.nodeName.match(/^(svg|a|span|h3)$/i)) {
            event.preventDefault();
            return true;
        }
    }
});

// Nest Has Children

function hasChildren(){
    $('.item-wrapper .nest').removeClass('has-children');
    $('.item-wrapper .nest').each(function(){
        $(this).has('.item-wrapper').addClass('has-children')
    });
}

hasChildren()

// Draggable Images

$(function () {
    $(".photos-wrapper").sortable({
        revert: true
    });
});

// Create Short URL

$('#short-url').keyup(function(){
    var shortURL = $('#short-url').val();
    $('.preview-url').text(shortURL);
})

$('#duplicate-url').keyup(function(){
    var duplicateURL = $('#duplicate-url').val();
    $('.duplicate-url-preview').text(duplicateURL);
})

// Initiating Codemirror
var editor = CodeMirror(document.getElementById("page-head"), {
    mode: "htmlmixed",
    theme: "blackboard",
    lineNumbers: true
});

var editor1 = CodeMirror(document.getElementById("page-body"), {
    mode: "htmlmixed",
    theme: "blackboard",
    lineNumbers: true
});
