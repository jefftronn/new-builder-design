$(document).ready(function(){

    // Color Picker Docs: https://www.eyecon.ro/colorpicker/#implement

    var colorPickerTrigger;
    var colorPickerInput;

    $('.color-picker-input .color-picker-trigger').click(function(e){
        colorPickerTrigger = $(this);
        colorPickerInput = $(this).siblings('input');
    });
    
    $('.color-picker-input .color-picker-trigger').each(function(){
        var input = $(this).parents('.color-picker-input').children('input');
        var initColor = input.val();
        $(this).css('backgroundColor', '#' + initColor);

        $(this).ColorPicker({
            color: initColor,
            onBeforeShow: function(){

            },
            onChange: function (hsb, hex, rgb) {
                $(colorPickerTrigger).css('backgroundColor', '#' + hex);
                $(colorPickerInput).val(hex);
                changeColor(colorPickerInput);
            },
            // livePreview: true
        });
    });

    $('#change-color-preview-bg').ColorPicker({
        color: '#f3f3f3',
        onChange: function (hsb, hex, rgb) {
            $('#color-preview-bg').css('backgroundColor', '#' + hex);
        }
    });

    $('.color-picker-input input').keyup(function(){
        var value = $(this).val().replace('#', '');
        var trigger = $(this).siblings('.color-picker-trigger');
        if(value.length >= 6){
            trigger.css('backgroundColor', '#' + value);
            $('.color-picker-input .color-picker-trigger').ColorPickerSetColor(value);
        }
    });

    // FONT COLORS

    function changeColor(element){
        var color = $(element).val()
        var attr = $(element).attr('data-change-attr');
        var hover = $(element).attr('data-hover');
        var changeTarget = $(element).attr('data-change-elem');
        if(!hover){
            $(changeTarget).css(attr, '#'+color);
        }
    }

    $('.color-picker-input input').keyup(function(){
        changeColor(this)
    });

    // Hoverable Colors
    $('.dropdown-link').mouseenter(function(){
        var color = $('#dropdown-link-hover-color').val();
        var bgColor = $('#dropdown-bg-hover-color').val();
        $(this).css({
            'background-color': '#'+bgColor,
            'color': '#'+color
        });
    });
    $('.dropdown-link').mouseout(function(){
        var color = $('#dropdown-link-color').val();
        var bgColor = $('#dropdown-bg-color').val();
        $(this).css({
            'background-color': '#'+bgColor,
            'color': '#'+color
        });
    });
    
    $('#button').mouseenter(function(){
        var color = $('#button-hover-color').val();
        var bgColor = $('#button-bg-hover-color').val();
        var borderColor = $('#button-border-hover-color').val();
        $(this).css({
            color: '#' + color,
            backgroundColor: '#' + bgColor,
            borderColor: '#' + borderColor
        });
    });
    $('#button').mouseout(function(){
        var color = $('#button-color').val();
        var bgColor = $('#button-bg-color').val();
        var borderColor = $('#button-border-color').val();
        $(this).css({
            color: '#' + color,
            backgroundColor: '#' + bgColor,
            borderColor: '#' + borderColor
        });
    });

    $('.menu-link').mouseenter(function(){
        var color = $('#menu-link-hover-color').val();
        $(this).css({
            color: '#' + color
        });
    });
    $('.menu-link').mouseout(function(){
        var color = $('#menu-link-color').val();
        $(this).css({
            color: '#' + color
        });
    });
   
    $('#link').mouseenter(function(){
        var color = $('#link-hover-color').val();
        $(this).css({
            color: '#' + color
        });
    });
    $('#link').mouseout(function(){
        var color = $('#link-color').val();
        $(this).css({
            color: '#' + color
        });
    });
});


//Change Font Families

$('body').on('click', '.header-font li', function(){
    var selectedFont = $(this).find('.item-label').text();
    console.log(selectedFont);
    $('#font-header').css('font-family', selectedFont);
});

$('body').on('click', '.body-font li', function(){
    var selectedFont = $(this).find('.item-label').text();
    console.log(selectedFont);
    $('#font-body').css('font-family', selectedFont);
});

//Change Font Weights

$('body').on('click', '.header-weight li', function(){
    var selectedWeight = $(this).find('.item-label').text();
    if (selectedWeight.includes('Regular')){
        selectedWeight = '400';
    } else{
        selectedWeight = '700';
    }
    $('#font-header').css('font-weight', selectedWeight);
});

$('body').on('click', '.body-weight li', function(){
    var selectedWeight = $(this).find('.item-label').text();
    if (selectedWeight.includes('Regular')){
        selectedWeight = '400';
    } else{
        selectedWeight = '100';
    }
    $('#font-body').css('font-weight', selectedWeight);
});

// Logo Dropify

$('.dropify').dropify()