//Initiating Codemirror

var editor = CodeMirror(document.getElementById("cc-global-head"), {
    mode: "htmlmixed",
    theme: "blackboard",
    lineNumbers: true
});

var editor1 = CodeMirror(document.getElementById("cc-global-body"), {
    mode: "htmlmixed",
    theme: "blackboard",
    lineNumbers: true
});

var editor2 = CodeMirror(document.getElementById("cc-custom-css"), {
    mode: "css",
    theme: "blackboard",
    lineNumbers: true
});