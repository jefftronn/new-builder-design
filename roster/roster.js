// ROSTER PAGE

// Sort Agents in Custom Order onclick

// function customSorting() {
//   if (document.getElementById("myRadio").checked = true) {
//     var list = document.getElementById("#agent-list");
//     $("#agent-list").sortable({
//         placeholder: 'highlight'
//     });
//     $( ".order-actions" ).css("display", "flex");
//     $( ".roster-order-radio" ).removeClass("open");
//     $(".roster-order-radio button").attr("aria-expanded", "false");
//   }
// }

// Sort Pages in Custom Order

$(".draggable-list").sortable({
  group: 'draggable-list',
  pullPlaceholder: false,
  // animation on drop
  onDrop: function ($item, container, _super) {
      var $clonedItem = $('<li/>').css({ height: 0 });
      $item.before($clonedItem);
      $clonedItem.animate({ 'height': $item.height() });

      $item.animate($clonedItem.position(), function () {
          $clonedItem.detach();
          _super($item, container);
      });
  },

  // set $item relative to cursor position
  onDragStart: function ($item, container, _super) {
      var offset = $item.offset(),
          pointer = container.rootGroup.pointer;

      adjustment = {
          left: pointer.left - offset.left,
          top: pointer.top - offset.top
      };

      _super($item, container);
  },
  onDrag: function ($item, position, e) {
      $item.css({
          left: position.left - adjustment.left,
          top: position.top - adjustment.top
      });
  },
  onMousedown: function ($item, _super, event) {
      if (!event.target.nodeName.match(/^(svg|a|span|h3)$/i)) {
          event.preventDefault();
          return true;
      }
  }
});

function addPhoneAgent(){
  $('#phone-details-grid').clone().appendTo('#phone-wrapper').fade();
};

function removePhoneAgent() {
  $('#phone-details-grid').remove();
}

function addEmailAgent(){
  $('#email-details-grid').clone().appendTo('#email-wrapper');
};

function removeEmailAgent() {
  $('#email-details-grid').remove();
}

function addAddressAgent(){
  $('#address-details-grid').clone().appendTo('#address-wrapper');
};

function removeAddressAgent() {
  $('#address-details-grid').remove();
}

function addPhoneAgentEdit(){
  $('#phone-details-grid-edit').clone().appendTo('#phone-wrapper-edit');
};

function removePhoneAgentEdit() {
  $('#phone-details-grid-edit').remove();
}

function addEmailAgentEdit(){
  $('#email-details-grid-edit').clone().appendTo('#email-wrapper-edit');
};

function removeEmailAgentEdit() {
  $('#email-details-grid-edit').remove();
}

function addAddressAgentEdit(){
  $('#address-details-grid-edit').clone().appendTo('#address-wrapper-edit');
};

function removeAddressAgentEdit() {
  $('#address-details-grid-edit').remove();
}

//Close Popup
$('.close-popup, .popup-overlay').click(function () {
  $('.side-popup').removeClass('open');
  $('.popup-overlay').removeClass('open');
  $('body, html').css('overflow', 'unset');
});