// Listing Page Switcher

$('.listing-type-switcher > div').click(function () {
    $('.listing-type-switcher > div').removeClass('active');
    $(this).addClass('active');

    if ($(this).hasClass('exclusive-listings-toggle')) {
        $('.exclusive-listings').removeClass('hide');
        $('.enhanced-listings').addClass('hide');
    } else {
        $('.exclusive-listings').addClass('hide');
        $('.enhanced-listings').removeClass('hide');
    }
});

// Select Enhanced Listing

$('body').on('click', '.select-listing', function () {
    var selectedText = $(this).find('span').text();
    if (selectedText === 'Select') {
        $(this).find('span').text('Selected');
    } else {
        $(this).find('span').text('Select');
    }
    $(this).parents('.listing-actions-wrapper').toggleClass('selected');

    if ($(this).parents('.listings').hasClass('enhanced-listings')) {
        if ($('.listing-actions-wrapper').hasClass('selected')) {
            $('.enhanced-listings-toolbar').addClass('open');
            $('body').css('padding-bottom', '50px');
        } else {
            $('.enhanced-listings-toolbar').removeClass('open');
            $('body').css('padding-bottom', '0');
        }
    } else {
        if ($('.listing-actions-wrapper').hasClass('selected')) {
            $('.exclusive-listings-toolbar').addClass('open');
            $('body').css('padding-bottom', '50px');
        } else {
            $('.exclusive-listings-toolbar').removeClass('open');
            $('body').css('padding-bottom', '0');
        }
    }
});

$('.bottom-toolbar .cancel, .listing-type-switcher').click(function () {
    $('.exclusive-listings-toolbar').removeClass('open');
    $('.select-listing span').text('Select');
    $('.listing-actions-wrapper').removeClass('selected');
    $('.bottom-toolbar').removeClass('open');
});