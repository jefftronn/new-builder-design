$(document).ready(function () {

    //Dismiss Callouts

    var clickedCallout;
    $('body').on('click', '.callout .close', function(){
        clickedCallout = $(this).parents('.callout')
        $(clickedCallout).remove();
    });

    // Append Carets too dropdowns

    function appendCarret(trigger) {
        trigger.append('<svg class="caret-down" xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="6 9 12 15 18 9"></polyline></svg>');
    }

    // Creates dropdown menu using tippy.
    function createDropdowns() {
        $('.dropdown').each(function () {
            if (!$(this).hasClass('dropdown-created')) {
                var dropdown = $(this);
                var button = $(dropdown).find('.dropdown-toggle');
                var trigger = this.querySelector('.dropdown-toggle');
                var menu = this.querySelector('.dropdown-menu').outerHTML;

                dropdown.addClass('dropdown-created');
                appendCarret(button);
                tippy(trigger, {
                    content: menu,
                    placement: 'bottom-start',
                    interactive: true,
                    interactiveBorder: 4,
                    trigger: "click",
                    arrow: false,
                    distance: 5,
                    onShow: function () {
                        $(dropdown).addClass('open')
                    },
                    onHide: function () {
                        $(dropdown).removeClass('open');
                        if ($(menu).children('.dropdown').length > 0) {
                            dropdownTip.disable()
                        }
                    }
                });
            }
        });
    }

    createDropdowns();

    $('body').on('click', '.dropdown', function (e) {
        if ($(this).find('.dropdown').length > 0) {
            createDropdowns();
        }
    });

    // Append Toast
    $('body').append('<div class="toast"> <span class="toast-message"></span> <div class="flex ai-center"> <div class="undo-toast">Undo</div><div class="close-toast"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg> </div></div></div>');
});

//Close Popup
$('.close-popup, .popup-overlay').click(function () {
    $('.popup').removeClass('open');
    $('.popup-overlay').removeClass('open');
    $('body, html').css('overflow', 'unset');
});

// Show Popup

function showPopup(id) {
    $('body, html').css('overflow', 'hidden');
    $('.side-popup').removeClass('open');
    $('#' + id + ', .popup-overlay').addClass('open');
    // hide any visible tippys
    var instance = document.querySelector('.tippy-popper')._tippy;
    instance.hide();
}

//Show Toast

function showToast(message, type, position) {
    $('.toast-message').text(message);
    if(type == 'undo'){
        setTimeout(function () {
            $('.toast').removeClass('show').removeClass(type).removeClass('center').removeClass(right);
        }, 20000)
    } else{
        setTimeout(function () {
            $('.toast').removeClass('show').removeClass(type).removeClass('undo').removeClass('center').removeClass(right);
        }, 3000)
    }
    if(position){
        $('.toast').addClass(position);
    };
    $('.toast').addClass('show').addClass(type);
}

$('body').on('click', '.close-toast, .undo-toast', function(){
    $('.toast').removeClass('show').removeClass('right').removeClass('center').removeClass('undo');
})

// Show Tabs

$('body').on('click', '.tab-trigger', function (e){
    var tabToShow = $(e.target).attr('data-tab');
    var tabGroup;
    if($(e.target).attr('data-tab-group')){
        tabGroup = $(e.target).attr('data-tab-group');
        var targetTab = $(e.target);
        $('.tab-pane').each(function(){
            if($(this).attr('data-tab-group') == tabGroup){
                $(this).removeClass('open')
            }
            $('#' + tabToShow).addClass('open');
        })
        $('.tab-trigger').each(function(){
            if($(this).attr('data-tab-group') == tabGroup){
                $(this).removeClass('active')
            }
            $(targetTab).addClass('active');
        })
    } else{
        $('.tab-pane').removeClass('open');
        $('#' + tabToShow).addClass('open');

        $('.tab-trigger').removeClass('active');
        $(this).addClass('active');
    }
});

//Switch

$('.switch').click(function () {
    $(this).toggleClass('active');
});

// Change Dropdown Text
$('body').on('click', '.dropdown-menu li', function () {
    var itemLabelText = $(this).find('.item-label').text();
    if (itemLabelText) {
        $('.tippy-active .toggle-label').text(itemLabelText)
    }
});

// Special Dropdown Item Types

$('body').on('click', '.dropdown-menu li', function (e) {
    var itemType = $(e.target).children().attr('class');
    
    // Checkbox
    if (itemType.includes('checkbox')) {
        var checkboxChild = $(this).find('input[type="checkbox"]');
        if (checkboxChild.prop('checked')) {
            checkboxChild.prop("checked", false);
        } else {
            checkboxChild.prop("checked", true);
        }
    }

    //Radio
    if (itemType.includes('radio')) {
        $(this).children('label.radio').children('input').prop('checked', true);
    }

    //Switch
    if (itemType.includes('switch')) {
        $(this).children('.switch').toggleClass('active');
    }
})
